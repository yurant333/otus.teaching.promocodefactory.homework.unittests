﻿using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Moq;
using Xunit;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Mvc;
using AutoFixture;
using AutoFixture.AutoMoq;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using FluentAssertions;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private PartnersController _partnerController;

        private Mock<IRepository<Partner>> _partnerRepositoryMock;

        private readonly DateTime NowDate = new DateTime(2023, 06, 14);

        public SetPartnerPromoCodeLimitAsyncTests() 
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnerController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

        }


        // Варианты Id партнеров для использования в Тестах
        Guid partnerId_1 = Guid.Parse("acf601a1-4363-451f-9cee-6db66630f70f");
        Guid partnerId_2 = Guid.Parse("daad1371-813d-4364-8888-771b8425a244");

        // 1. Если партнер не найден, то возвращаем ошибку 404;
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotFound_ReturnsNotFound()
        {

            //Arrange
            Partner partner = null;

            var request = new PartnerPromoCodeLimitRequestBulider().Build();

            _partnerRepositoryMock.Setup(p => p.GetByIdAsync(partnerId_1)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partnerId_1, request);
            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();

        }

        // 2. Если партнер заблокирован (IsActive=false в классе Partner), то возвращаем ошибку 400.
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsyncTests_PartnerIsNotActive_ReturnsBadRequest()
        {

            //Arrange
            var partner = new PartnerBuilder(partnerId_2).IsActive(false).Build();

            var request = new PartnerPromoCodeLimitRequestBulider().Build();

            _partnerRepositoryMock.Setup(p => p.GetByIdAsync(partnerId_2)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partnerId_2, request);
            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        // 3. Если партнеру выставляется лимит, то обнуляем количество промокодов, которые партнер выдал NumberIssuedPromoCodes.
        // Если лимит закончился, то количество не обнуляем.
        [Fact]
        public async void SetPartnerPromoCodeLimitAsyncTests_PartnerIsGivenLimit_ReturnsBadRequest()
        {
            
            //Arrange
            var partner = new PartnerBuilder(partnerId_2).Build();

            var request = new PartnerPromoCodeLimitRequestBulider().Build();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);

        }

        // 4. Если партнеру выставляется лимит, то обнулем количество промокодов, которые партнер выдал NumberIssuedPromoCodes.
        // Если лимит закончился, то количество не обнуляется.

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsGivenLimit_NotResetIssuedPromocodes()
        {
            //Arrange
            var partner = new PartnerBuilder(partnerId_2).WithEmptyPromoCodeLimits().WithPromocodesCount(1).Build();

            var request = new PartnerPromoCodeLimitRequestBulider().Build();
            
            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();

            partner.NumberIssuedPromoCodes.Should().Be(1);
        }

        // 5. При установке лимита нужно отключить предыдущий лимит.
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_OffPreviousLimit()
        {
            //Arrange
            var request = new PartnerPromoCodeLimitRequestBulider().Build();
            
            var partner = new PartnerBuilder(partnerId_2).WithCancelDate(NowDate).Build();

            var partnerLimit = partner.PartnerLimits.First();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partnerId_2, request);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partnerLimit.CancelDate.Should().Be(NowDate);
        }

        // 6. Лимит должен быть больше 0.
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitNegative_ReturnBadRequest()
        {

            //Arrange
            var request = new PartnerPromoCodeLimitRequestBulider().WithLimit(-1).Build();

            var partner = new PartnerBuilder(partnerId_2).Build();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            //Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        // 7. Нужно убедиться, что сохраненный новый лимит попал в базу данных.

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimit_AddNewLimitIntoDb()
        {
            //Arrange
            var request = new PartnerPromoCodeLimitRequestBulider().Build();

            var partner = new PartnerBuilder(partnerId_2).Build();

            var partnerLimit = partner.PartnerLimits.First();

            _partnerRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            _partnerRepositoryMock.Verify(p => p.UpdateAsync(partner), Times.Once);
        }
    }
}