﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerBuilder
    {
        private readonly Partner _partner;
        public PartnerBuilder(Guid guid) 
        {
            _partner = new Partner()
            {
                Id = guid,
                Name = "Рога и копыта",
                IsActive = true,
                NumberIssuedPromoCodes = 19,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("f784076b-7132-46dd-a285-d613a858195b"),
                        CreateDate = new DateTime(2023, 06, 7),
                        EndDate = new DateTime(2023, 10, 9),
                        Limit = 97,
                    }
                }
            };
        }

        public PartnerBuilder IsActive(bool isActive = true)
        {
            _partner.IsActive = isActive;
            return this;
        }

        public PartnerBuilder WithEmptyPromoCodeLimits()
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>();
            return this;
        }

        public PartnerBuilder WithPromocodesCount(int numberIssuedPromocodes)
        {
            _partner.NumberIssuedPromoCodes = numberIssuedPromocodes;
            return this;
        }

        public PartnerBuilder WithCancelDate(DateTime cancelDate)
        {
            _partner.PartnerLimits.First().CancelDate = cancelDate;
            return this;
        }
        
        public Partner Build()
        {
            return _partner;
        }
    }
}