﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    class PartnerLimitBuilder
    {
        private readonly PartnerPromoCodeLimit _partnerLimit;

        public PartnerLimitBuilder()
        {
            _partnerLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("cd12a65f-4bee-4d49-afb3-93b91682c862"),
                CreateDate = new DateTime(2023, 06, 7),
                EndDate = new DateTime(2023, 10, 9),
                CancelDate = null,
                Limit = 10,
            };
        }
    }
}