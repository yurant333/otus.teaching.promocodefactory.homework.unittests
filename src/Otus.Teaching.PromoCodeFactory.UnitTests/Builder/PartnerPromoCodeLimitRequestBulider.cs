﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    class PartnerPromoCodeLimitRequestBulider
    {
        private readonly SetPartnerPromoCodeLimitRequest _request;

        public PartnerPromoCodeLimitRequestBulider()
        {
            _request= new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = new DateTime(2023, 10, 9),
                Limit = 3
            };
        }

        public PartnerPromoCodeLimitRequestBulider WithLimit(int limit)
        {
            _request.Limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return _request;
        }
    }
}